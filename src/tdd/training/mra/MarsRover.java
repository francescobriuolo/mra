package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {

	protected int planetX;
	protected int planetY;
	protected List<String> planetObstacles;
	protected List<String> encounteredObstacles;
	protected String landingPosition;
	protected String position;
	
	protected enum CardinalPoint{
		N,
		S,
		E,
		W
	}
	
	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planetX = planetX;
		this.planetY = planetY;
		this.planetObstacles = planetObstacles;
		this.encounteredObstacles = new ArrayList<String>();
		this.position = ""+CardinalPoint.N+"";
		this.landingPosition = "(0,0," + position + ")";
		
		if(planetX < 0 || planetY < 0)
			throw new MarsRoverException("Invalid planet");
	}
	
	/**
	 * It return the current landing position of the rover on the planet.
	 */
	public String getCurrentPosition() {
		return this.landingPosition;
	}
	
	/**
	 * It sets the current landing position of this rover on the planet.
	 *
	 * @param landingPosition The current landing position.
	 */
	public void setCurrentPosition(String landingPosition) {
		this.landingPosition = landingPosition;
	}
	

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		boolean flag = false;
		String obstacle = "";
		if(!this.planetObstacles.isEmpty()) {
			for(int i = 0; i < this.planetObstacles.size(); i++) {
				obstacle = "("+x+","+y+")";
				if(planetObstacles.get(i).equals(obstacle)) {
			        flag = true;
				}
			}
		}
		return flag;
	}
	
	/**
	 * It returns the obstacle.
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public String getObstacleAt(int x, int y) {
		for(int i=0;i<planetObstacles.size();i++) {
			String obstacle="("+x+","+y+")";
			if(planetObstacles.get(i).equals(obstacle)) {
				return obstacle;
			}
		}
		return null;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		int positionX = 0;
		int positionY = 0;
		int newPositionX = 0;
		int newPositionY = 0;
		position = this.getCurrentPosition().substring(5, 6);
		char command = '\0';
		if(commandString.equals("")) {
			this.setCurrentPosition(landingPosition);
		}
		else {
			for(int i = 0; i < commandString.length(); i++) {
				positionX = Integer.parseInt(this.landingPosition.substring(1, 2));
				positionY = Integer.parseInt(this.landingPosition.substring(3, 4));
				System.out.println(commandString.charAt(i));
				switch(command = commandString.charAt(i)) {
					case 'r':
						if(position.equals("N")) {
							position = ""+CardinalPoint.E+"";
						}
						else if(position.equals("E")) {
							position = ""+CardinalPoint.S+"";
						}
						else if(position.equals("S")) {
							position = ""+CardinalPoint.W+"";
						}
						else if(position.equals("W")) {
							position = ""+CardinalPoint.N+"";
						}
						this.setCurrentPosition("("+positionX+","+positionY+","+position+")" + this.encounteredObstacles);
						break;
					case 'l':
						if(position.equals("N")) {
							position = ""+CardinalPoint.W+"";
						}
						else if(position.equals("E")) {
							position = ""+CardinalPoint.N+"";
						}
						else if(position.equals("S")) {
							position = ""+CardinalPoint.E+"";
						}
						else if(position.equals("W")) {
							position = ""+CardinalPoint.S+"";
						}
						this.setCurrentPosition("("+positionX+","+positionY+","+position+")" + this.encounteredObstacles);
						break;
					case 'f':
						if(position.equalsIgnoreCase("E")) {
							if(positionX == (this.planetX - 1) ) {
								newPositionX = 0;
							}
							else {
								newPositionX = positionX + 1;
							}
							if(this.planetContainsObstacleAt(newPositionX, positionY)) {
								if(!this.encounteredObstacles.contains("("+newPositionX+","+positionY+")")) {
									this.encounteredObstacles.add("("+newPositionX+","+positionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+(newPositionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
						}
						else if(position.equalsIgnoreCase("N")) {
							if(positionY == (this.planetY - 1) ) {
								newPositionY = 0;
							}
							else {
								newPositionY = positionY + 1;
							}
							if(this.planetContainsObstacleAt(positionX, newPositionY)) {
								if(this.encounteredObstacles.contains("("+positionX+","+newPositionY+")")) {
									this.encounteredObstacles.add("("+positionX+","+newPositionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+positionX+","+(newPositionY)+","+position+")" + this.encounteredObstacles);
							}
						}
						else if(position.equalsIgnoreCase("S")) {
							if(positionY == 0 ) {
								newPositionY = this.planetY - 1;
							}
							else {
								newPositionY = positionY - 1;
							}
							if(this.planetContainsObstacleAt(positionX, newPositionY)) {
								if(this.encounteredObstacles.contains("("+positionX+","+newPositionY+")")) {
									this.encounteredObstacles.add("("+positionX+","+newPositionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+positionX+","+(newPositionY)+","+position+")" + this.encounteredObstacles);
							}
						}
						else if(position.equalsIgnoreCase("W")) {
							if(positionX == (0) ) {
								newPositionX = this.planetX - 1;
							}
							else {
								newPositionX = positionX - 1;
							}
							if(this.planetContainsObstacleAt(newPositionX, positionY)) {
								if(!this.encounteredObstacles.contains("("+newPositionX+","+positionY+")")) {
									this.encounteredObstacles.add("("+newPositionX+","+positionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+(newPositionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
						}
						break;
					case 'b':
						if(position.equalsIgnoreCase("E")) {
							if(positionX == 0 ) {
								newPositionX = this.planetX - 1;
							}
							else {
								newPositionX = positionX - 1;
							}
							if(this.planetContainsObstacleAt(newPositionX, positionY)) {
								if(!this.encounteredObstacles.contains("("+newPositionX+","+positionY+")")) {
									this.encounteredObstacles.add("("+newPositionX+","+positionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
								}
							else {
								this.setCurrentPosition("("+(newPositionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
						}
						else if(position.equalsIgnoreCase("N")) {
							if(positionY == 0 ) {
								newPositionY = this.planetY - 1;
							}
							else {
								newPositionY = positionY - 1;
							}
							if(this.planetContainsObstacleAt(positionX, newPositionY)) {
								if(!this.encounteredObstacles.contains("("+positionX+","+newPositionY+")")) {
									this.encounteredObstacles.add("("+positionX+","+newPositionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+positionX+","+(newPositionY)+","+position+")" + this.encounteredObstacles);
							}
						}
						else if(position.equalsIgnoreCase("S")) {
							if(positionY == (this.planetY - 1) ) {
								newPositionY = 0;
							}
							else {
								newPositionY = positionY + 1;
							}
							if(this.planetContainsObstacleAt(positionX, newPositionY)) {
								if(this.encounteredObstacles.contains("("+positionX+","+newPositionY+")")) {
									this.encounteredObstacles.add("("+positionX+","+newPositionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+positionX+","+(newPositionY)+","+position+")" + this.encounteredObstacles);
							}
						}
						else if(position.equalsIgnoreCase("W")) {
							if(positionX == (this.planetX - 1) ) {
								newPositionX = 0;
							}
							else {
								newPositionX = positionX - 1;
							}
							if(this.planetContainsObstacleAt(newPositionX, positionY)) {
								if(!this.encounteredObstacles.contains("("+newPositionX+","+positionY+")")) {
									this.encounteredObstacles.add("("+newPositionX+","+positionY+")");
								}
								this.setCurrentPosition("("+(positionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
							else {
								this.setCurrentPosition("("+(newPositionX)+","+positionY+","+position+")" + this.encounteredObstacles);
							}
						}
						break;
				}
				//this.encounteredObstacles.forEach(System.out::print);
				System.out.println("Passo "+i+": "+this.getCurrentPosition().toString().replace("[", "").replace("]", "").replace(", ", ""));
			}
		}
		return this.getCurrentPosition().toString().replace("[", "").replace("]", "").replace(", ", "");
	}

}
