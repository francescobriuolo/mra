package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	// User story 1
	@Test
	public void testMarsRover() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			assertTrue(rover.planetContainsObstacleAt(2,3));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 2
	@Test
	public void testMarsRoverFacingNorth() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
					
			boolean obstacle = rover.planetContainsObstacleAt(2,3);
			
			String commandString = "";
			assertEquals("(0,0,N)", rover.executeCommand(commandString));			
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 3
	@Test
	public void testMarsRoverTurningRight() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "r";
			assertEquals("(0,0,E)", rover.executeCommand(commandString));				
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 3
	@Test
	public void testMarsRoverTurningLeft() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "l";
			assertEquals("(0,0,W)", rover.executeCommand(commandString));				
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 4
	@Test
	public void testMarsRoverMovingForward() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			rover.setCurrentPosition("(7,6,N)");
			
			String commandString = "f";
			assertEquals("(7,7,N)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 5
	@Test
	public void testMarsRoverMovingBackward() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			rover.setCurrentPosition("(5,8,E)");
			
			String commandString = "b";
			assertEquals("(4,8,E)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 6
	@Test
	public void testMarsRoverMovingCombined() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "ffrff";
			assertEquals("(2,2,E)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 7
	@Test
	public void testMarsRoverWrapping() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "b";
			assertEquals("(0,9,N)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 7
	@Test
	public void testMarsRoverWrappingSouth() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			rover.setCurrentPosition("(0,9,S)");
			String commandString = "b";
			assertEquals("(0,0,S)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 7
	@Test
	public void testMarsRoverWrappingWest() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			rover.setCurrentPosition("(9,5,W)");
			String commandString = "b";
			assertEquals("(0,5,W)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 7
	@Test
	public void testMarsRoverWrappingEast() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(4,7)");
			planetObstacles.add("(2,3)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			rover.setCurrentPosition("(0,5,E)");
			String commandString = "b";
			assertEquals("(9,5,E)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 8
	@Test
	public void testMarsRoverSingleObstacle() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(2,2)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "ffrfff";
			assertEquals("(1,2,E)(2,2)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 9
	@Test
	public void testMarsRoverMultipleObstacle() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(2,2)");
			planetObstacles.add("(2,1)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "ffrfffrflf";
			assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 10
	@Test
	public void testMarsRoverWrappingAndObstacles() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(0,9)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "b";
			assertEquals("(0,0,N)(0,9)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
	
	// User story 11
	@Test
	public void testMarsRoverTourAroundThePlanet() throws MarsRoverException {
		int planetX = 6;
		int planetY = 6;
		List<String> planetObstacles = new ArrayList<String>();
		try {
			planetObstacles.add("(2,2)");
			planetObstacles.add("(0,5)");
			planetObstacles.add("(5,0)");
			MarsRover rover = new MarsRover(planetX, planetY, planetObstacles);
			
			String commandString = "ffrfffrbbblllfrfrbbl";
			assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand(commandString));
		}catch(MarsRoverException mre) {
			System.err.println("Invalid planet");
		}
	}
}
